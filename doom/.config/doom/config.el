;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Ian S. Pringle"
      user-mail-address "ian@dapringles.com")

(setq doom-theme 'doom-moonlight)

;; Auto-save
(add-hook 'auto-save-hook 'org-save-all-org-buffers)

;; Much of my org setup was stolen from http://doc.norang.ca/org-mode.html
(setq org-directory "~/org/")
(setq org-agenda-files '("~/org/" "~/.org-jira/"))
(after! org
  (setq org-todo-keywords
      '((sequence "TODO(t)"
                  "NEXT(n)"
                  "|"
                  "DONE(d)")
        (sequence "WAIT(w@/!)"
                  "HOLD(h@/!)"
                  "|"
                  "KILL(c@/!)"
                  "CALL"
                  "MEET")))

  (setq org-todo-keyword-faces
        '(("TODO" :foreground "#ff757f" :weight bold)
          ("NEXT" :foreground "#82aaff" :weight bold)
          ("DONE" :foreground "c3e88d" :weight bold)
          ("WAIT" :foreground "c099ff" :weight bold)
          ("HOLD" :foreground "magenta" :weight bold)
          ("KILL" :foreground "c3e88d" :weight bold)
          ("MEET" :foreground "c3e88d" :weight bold)
          ("CALL" :foreground "c3e88d" :weight bold)))

  (setq org-use-fast-todo-selection t)
  (setq org-treat-S-cursor-todo-selection-as-state-change nil)
  (setq org-todo-state-tags-triggers
      '(("KILL" ("KILL" . t))
        ("WAIT" ("WAIT" . t))
        ("HOLD" ("WAIT") ("HOLD" . t))
        (done ("WAIT") ("HOLD"))
        ("TODO" ("WAIT") ("KILL") ("HOLD"))
        ("NEXT" ("WAIT") ("KILL") ("HOLD"))
        ("DONE" ("WAIT") ("KILL") ("HOLD"))))

  (setq +org-capture-inbox "~/org/inbox.org")
  (setq org-capture-templates
        '(("t" "Todo" entry (file +org-capture-inbox)
           "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)

          ("r" "Respond" entry (file +org-capture-inbox)
           "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n"
           :clock-in t :clock-resume t :immediate-finish t)

          ("n" "Note" entry (file +org-capture-inbox)
           "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)

          ("w" "Org-protocol" entry (file +org-capture-inbox)
           "* TODO Review %c\n%U\n" :immediate-finish t)

          ("m" "Meeting" entry (file +org-capture-inbox)
           "* MEET with %? :MEET:\n%U" :clock-in t :clock-resume t)

          ("p" "Phone call" entry (file +org-capture-inbox)
           "* CALL %? :CALL:\n%U" :clock-in t :clock-resume t)

          ("h" "Habit" entry (file +org-capture-inbox)
           "* NEXT %?\n%U\n%a\nSCHEDULED: %<<%Y-%m-%d %a .+1d/3d>>\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

(setq org-directory "~/docs/org/")

;; Set the directory for `deft' to the same location as `org'
(setq deft-directory "~/docs/org/")

;; set capture templates
(setq org-capture-templates '(("t" "Todo [inbox]" entry
                               (file+headline "~/docs/org/todo/inbox.org" "Tasks")
                               "* TODO %i%?")
                              ("T" "Reminder" entry
                               (file+headline "~/docs/org/todo/reminder.org" "Reminder")
                               "* %i%? \n %U")
                              ("j" "Journal" entry
                               (file+headline "~/docs/org/journal.org" "Journal")
                               "* %?\nEntered on %U\n %i\n %a")
                              ("k" "Knowledge" entry
                               (file+headline "~/docs/org/kb/inbox.org" "Knowledge")
                               "* %?\n %i\n")
                              )
      )

;; Set keywords and colors for hl-todo
;; A lot of these are right out of the src, just putting them here to document all keywords
 (setq hl-todo-keyword-faces
  '(("HOLD" . "#d0bf8f")
    ("TODO" . "#cc9393")
    ("NEXT" . "#dca3a3")
    ("THEM" . "#dc8cc3")
    ("PROG" . "#7cb8bb")
    ("OKAY" . "#7cb8bb")
    ("DONT" . "#5f7f5f")
    ("FAIL" . "#8c5353")
    ("DONE" . "#afd8af")
    ("CAND" . "#3d2f2f")
    ("NOTE"   . "#d0bf8f")
    ("KLUDGE" . "#d0bf8f")
    ("HACK"   . "#d0bf8f")
    ("TEMP"   . "#d0bf8f")
    ("FIXME"  . "#cc9393")
    ("XXX+"   . "#cc9393"))
  )

;; set custom filter for agenda
(setq org-agenda-custom-commands
      '(("w" "Work-related" tags-todo ":work:"
         ((org-agenda-overriding- "Work"))
         ))
      )


;; include the habit module for reoccuring tasks
(add-to-list 'org-modules 'org-habits)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(use-package! org-jira)
(after! org-jira  (setq jiralib-url "https://jira.os.liberty.edu"))

;; (sky-color-clock-initialize 37)
;; (sky-color-clock-initialize-openweathermap-client "b587ec8414a6100be44beec5ae9cfe05" 4771075)
;; (setq global-mode-string (push '(:eval (sky-color-clock)) (default-value 'mode-line-format)))
